using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace SHIFR
{
	public delegate void KeyTranformation(ref string x);
	public partial class Form1 : Form
	{
		private const int sizeOfSymbol = 8;// ���-�� ����� �� ���� ������
		public KeyTranformation keytransform;
		private const int sizeOfBlockByte = 64; //� DES ������ ����� 64 ���, �� ��������� � unicode ������ � ��� ���� ������, �� �������� ���� ���� � ��� ����
		private int sizeOfBlock;//
		public string S { get; set; }// �������� ������
		int code = 1251;//������� ����� ���������
		public delegate void KeyTranformation(ref string x);
		public String Key;//���� � ����������
		public String[] Blocks { get; set; }//����� �������
		public String[] SBlocks { get; set; }// S ����� �����
		public String[,] KEY { get; set; }// S ����� �����

		public static int[] ip = new int[64] { 58, 50, 42, 34, 26, 18, 10, 2,
										60, 52, 44, 36, 28, 20, 12, 4,
										62, 54, 46, 38, 30, 22, 14, 6,
										64, 56, 48, 40, 32, 24, 16, 8,
										57, 49, 41, 33, 25, 17,  9, 1,
										59, 51, 43, 35, 27, 19, 11, 3,
										61, 53, 45, 37, 29, 21, 13, 5,
										63, 55, 47, 39, 31, 23, 15, 7 };

		static int[] final_ip = new int[64] {           40, 8, 48, 16, 56, 24, 64, 32,
														39, 7, 47, 15, 55, 23, 63, 31,
														38, 6, 46, 14, 54, 22, 62, 30,
														37, 5, 45, 13, 53, 21, 61, 29,
														36, 4, 44, 12, 52, 20, 60, 28,
														35, 3, 43, 11, 51, 19, 59, 27,
														34, 2, 42, 10, 50, 18, 58, 26,
														33, 1, 41,  9, 49, 17, 57, 25 };
		static int[] C0D0 = new int[56] {                57, 49, 41, 33, 25, 17, 9,
														1, 58, 50, 42, 34, 26, 18,
														10,2, 59, 51, 43, 35, 27,
														19, 11, 3, 60, 52, 44, 36,
														63, 55, 47, 39, 31, 23, 15,
														7, 62, 54, 46, 38, 30, 22,
														14, 6, 61, 53, 45, 37, 29,
														21, 13, 5,  28, 20, 12, 4 };
		static int[] expansion = new int[48] {  32,  1,  2,  3,  4,  5,  4,  5,
												 6,  7,  8,  9,  8,  9, 10, 11,
												12, 13, 12, 13, 14, 15, 16, 17,
												16, 17, 18, 19, 20, 21, 20, 21,
												22, 23, 24, 25, 24, 25, 26, 27,
												28, 29, 28, 29, 30, 31, 32,  1 };
		static int[] shiftkey = new int[48] {  14, 17, 11, 24, 1, 5,
											  3 , 28, 15,6 ,21,10,
											  23, 19, 12, 4, 26, 8,
											  16, 7, 27, 20, 13, 2,
											  41, 52, 31, 37, 47, 55,
											  30, 40, 51, 45, 33, 48,
											  44, 49, 39,56, 34, 53,
											  46, 42, 50, 36, 29, 32};
		static int[] p = new int[32] {   16,  7, 20, 21, 29, 12, 28, 17,
										 1, 15, 23, 26,  5, 18, 31, 10,
										 2,  8, 24, 14, 32, 27,  3,  9,
										 19, 13, 30,  6, 22, 11,  4, 25};
		static int[] Shiftkey = new int[16] { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };

		static byte[,,] s_block = new byte[8, 4, 16]{
							   {{0x0e, 0x04, 0x0d, 0x01, 0x02, 0x0f, 0x0b, 0x08, 0x03, 0x0a, 0x06, 0x0c, 0x05, 0x09, 0x00, 0x07},
								{0x00, 0x0f, 0x07, 0x04, 0x0e, 0x02, 0x0d, 0x01, 0x0a, 0x06, 0x0c, 0x0b, 0x09, 0x05, 0x03, 0x08},
								{0x04, 0x01, 0x04, 0x08, 0x0d, 0x06, 0x02, 0x0b, 0x0f, 0x0c, 0x09, 0x07, 0x03, 0x0a, 0x05, 0x00},
								{0x0f, 0x0c, 0x08, 0x02, 0x04, 0x09, 0x01, 0x07, 0x05, 0x0b, 0x03, 0x0e, 0x0a, 0x00, 0x06, 0x0d}},
							   {{0x0f, 0x01, 0x08, 0x0e, 0x06, 0x0b, 0x03, 0x04, 0x09, 0x07, 0x02, 0x0d, 0x0c, 0x00, 0x05, 0x0a},
								{0x03, 0x0d, 0x04, 0x07, 0x0f, 0x02, 0x08, 0x0e, 0x0c, 0x00, 0x01, 0x0a, 0x06, 0x09, 0x0b, 0x05},
								{0x00, 0x0e, 0x07, 0x0b, 0x0a, 0x04, 0x0d, 0x01, 0x05, 0x08, 0x0c, 0x06, 0x09, 0x03, 0x02, 0x0f},
								{0x0d, 0x08, 0x0a, 0x01, 0x03, 0x0f, 0x04, 0x02, 0x0b, 0x06, 0x07, 0x0c, 0x00, 0x05, 0x0e, 0x09}},
							   {{0x0a, 0x00, 0x09, 0x0e, 0x06, 0x03, 0x0f, 0x05, 0x01, 0x0d, 0x0c, 0x07, 0x0b, 0x04, 0x02, 0x08},
								{0x0d, 0x07, 0x00, 0x09, 0x03, 0x04, 0x06, 0x0a, 0x02, 0x08, 0x05, 0x0e, 0x0c, 0x0b, 0x0f, 0x01},
								{0x0d, 0x06, 0x04, 0x09, 0x08, 0x0f, 0x03, 0x00, 0x0b, 0x01, 0x02, 0x0c, 0x05, 0x0a, 0x0e, 0x07},
								{0x01, 0x0a, 0x0d, 0x00, 0x06, 0x09, 0x08, 0x07, 0x04, 0x0f, 0x0e, 0x03, 0x0b, 0x05, 0x02, 0x0c}},
							   {{0x07, 0x0d, 0x0e, 0x03, 0x00, 0x06, 0x09, 0x0a, 0x01, 0x02, 0x08, 0x05, 0x0b, 0x0c, 0x04, 0x0f},
								{0x0d, 0x08, 0x0b, 0x05, 0x06, 0x0f, 0x00, 0x03, 0x04, 0x07, 0x02, 0x0c, 0x01, 0x0a, 0x0e, 0x09},
								{0x0a, 0x06, 0x09, 0x00, 0x0c, 0x0b, 0x07, 0x0d, 0x0f, 0x01, 0x03, 0x0e, 0x05, 0x02, 0x08, 0x04},
								{0x03, 0x0f, 0x00, 0x06, 0x0a, 0x01, 0x0d, 0x08, 0x09, 0x04, 0x05, 0x0b, 0x0c, 0x07, 0x02, 0x0e}},
							   {{0x02, 0x0c, 0x04, 0x01, 0x07, 0x0a, 0x0b, 0x06, 0x08, 0x05, 0x03, 0x0f, 0x0d, 0x00, 0x0e, 0x09},
								{0x0e, 0x0b, 0x02, 0x0c, 0x04, 0x07, 0x0d, 0x01, 0x05, 0x00, 0x0f, 0x0a, 0x03, 0x09, 0x08, 0x06},
								{0x04, 0x02, 0x01, 0x0b, 0x0a, 0x0d, 0x07, 0x08, 0x0f, 0x09, 0x0c, 0x05, 0x06, 0x03, 0x00, 0x0e},
								{0x0b, 0x08, 0x0c, 0x07, 0x01, 0x0e, 0x02, 0x0d, 0x06, 0x0f, 0x00, 0x09, 0x0a, 0x04, 0x05, 0x03}},
							   {{0x0c, 0x01, 0x0a, 0x0f, 0x09, 0x02, 0x06, 0x08, 0x00, 0x0d, 0x03, 0x04, 0x0e, 0x07, 0x05, 0x0b},
								{0x0a, 0x0f, 0x04, 0x02, 0x07, 0x0c, 0x09, 0x05, 0x06, 0x01, 0x0d, 0x0e, 0x00, 0x0b, 0x03, 0x08},
								{0x09, 0x0e, 0x0f, 0x05, 0x02, 0x08, 0x0c, 0x03, 0x07, 0x00, 0x04, 0x0a, 0x01, 0x0d, 0x01, 0x06},
								{0x04, 0x03, 0x02, 0x0c, 0x09, 0x05, 0x0f, 0x0a, 0x0b, 0x0e, 0x01, 0x07, 0x06, 0x00, 0x08, 0x0d}},
							   {{0x04, 0x0b, 0x02, 0x0e, 0x0f, 0x00, 0x08, 0x0d, 0x03, 0x0c, 0x09, 0x07, 0x05, 0x0a, 0x06, 0x01},
								{0x0d, 0x00, 0x0b, 0x07, 0x04, 0x09, 0x01, 0x0a, 0x0e, 0x03, 0x05, 0x0c, 0x02, 0x0f, 0x08, 0x06},
								{0x01, 0x04, 0x0b, 0x0d, 0x0c, 0x03, 0x07, 0x0e, 0x0a, 0x0f, 0x06, 0x08, 0x00, 0x05, 0x09, 0x02},
								{0x06, 0x0b, 0x0d, 0x08, 0x01, 0x04, 0x0a, 0x07, 0x09, 0x05, 0x00, 0x0f, 0x0e, 0x02, 0x03, 0x0c}},
							   {{0x0d, 0x02, 0x08, 0x04, 0x06, 0x0f, 0x0b, 0x01, 0x0a, 0x09, 0x03, 0x0e, 0x05, 0x00, 0x0c, 0x07},
								{0x01, 0x0f, 0x0d, 0x08, 0x0a, 0x03, 0x07, 0x04, 0x0c, 0x05, 0x06, 0x0b, 0x00, 0x0e, 0x09, 0x02},
								{0x07, 0x0b, 0x04, 0x01, 0x09, 0x0c, 0x0e, 0x02, 0x00, 0x06, 0x0a, 0x0d, 0x0f, 0x03, 0x05, 0x08},
								{0x02, 0x01, 0x0e, 0x07, 0x04, 0x0a, 0x08, 0x0d, 0x0f, 0x0c, 0x09, 0x00, 0x03, 0x05, 0x06, 0x0b}}};

		public Form1()

		{
			keytransform = new KeyTranformation(FullKeyEndByte);
			keytransform += CorrectKey;
			keytransform += Perestanovka_key;
			Key = " ";
			KEY = new string[1000, 16];
			S = " ";
			InitializeComponent();
			sizeOfBlock = sizeOfBlockByte / sizeOfSymbol;
		}
		private void Perestanovka_key(ref string input)
		{
			StringBuilder output = new StringBuilder(input);
			for (int i = 0; i < 56; i++)
			{
				output[i] = input[C0D0[i] - 1];
			}
			input = output.ToString();
		}
		private string Perestanovka_key_2(string input)
		{
			StringBuilder output = new StringBuilder(input);
			for (int i = 0; i < 48; i++)
			{
				output[i] = input[shiftkey[i] - 1];
			}
			return output.ToString();
		}
		private string beginIP(string input)

		{
			StringBuilder output = new StringBuilder(input);
			for (int i = 0; i < sizeOfBlockByte; i++)
			{
				output[i] = input[ip[i] - 1];
			}
			return output.ToString();
		}
		private string Work_S_block(string input, int Snumber)
		{
			int a, b;
			string output = input[0].ToString() + input[5].ToString();
			a = Convert.ToInt32(output, 2);
			b = Convert.ToInt32(input.Substring(1, 4), 2);
			a = s_block[Snumber, a, b];

			output = Convert.ToString(a, 2);
			while (output.Length < 4)
			{
				output = "0" + output;
			}
			return output;
		}
		private string shiftP(string input)

		{
			StringBuilder output = new StringBuilder(input);
			for (int i = 0; i < 32; i++)
			{
				output[i] = input[p[i] - 1];
			}
			return output.ToString();
		}
		private string EndIP(string input)

		{
			StringBuilder output = new StringBuilder(input);
			for (int i = 0; i < sizeOfBlockByte; i++)
			{
				output[i] = input[final_ip[i] - 1];
			}
			return output.ToString();
		}
		private string Expansion(string input)
		{
			StringBuilder output = new StringBuilder(input + input);
			for (int i = 0; i < 48; i++)
			{
				output[i] = input[expansion[i] - 1];

			}
			string odut = output.ToString();
			return odut.Substring(0, 48);
		}
		private void Form1_Load(object sender, EventArgs e)
		{

			richTextBox1.Text = S;
			richTextBox5.Text = Key;
		}
		private void StringToSBlocks(string input)
		{
			int size = 6;
			SBlocks = new string[input.Length / size];

			for (int i = 0; i < SBlocks.Length; i++)
			{
				SBlocks[i] = input.Substring(i * size, size);
			}
		}

		private string StringToNormalLength(string input)
		{
			while (input.Length % sizeOfBlock != 0)
			{
				input += "#";
			}
			return input;
		}
		private string KeyToNextRound(string input, int l)
		{
			string L = input.Substring(0, input.Length / 2);
			string R = input.Substring(input.Length / 2, input.Length / 2);
			for (int i = 0; i < Shiftkey[l]; i++)
			{
				L = L[L.Length - 1] + L;
				L = L.Remove(L.Length - 1);

				R = R[R.Length - 1] + R;
				R = R.Remove(R.Length - 1);
			}
			return L + R;
		}
		private string KeyToPrevRound(string input, int l)
		{
			string L = input.Substring(0, input.Length / 2);
			string R = input.Substring(input.Length / 2, input.Length / 2);
			for (int i = 0; i < Shiftkey[l]; i++)
			{
				L = L + L[0];
				L = L.Remove(0, 1);
				R = R + R[0];
				R = R.Remove(0, 1);
			}

			return L + R;
		}
		private void StringToBlocks(string input)
		{
			//����� ����������� ������ �� ����� � ����������� � � �������� ������
			Blocks = new string[input.Length / sizeOfBlock];

			for (int i = 0; i < Blocks.Length; i++)
			{

				Blocks[i] = input.Substring(i * sizeOfBlock, sizeOfBlock);
				Blocks[i] = StringToByteFormat(Blocks[i]);
			}
		}

		private void FullKeyEndByte(ref string key)
		{
			//������� ���� �� ������ �����
			while (key.Length < (sizeOfBlock))
			{
				key = "G" + key;
			}
			if (key.Length > (sizeOfBlock))
				key = key.Substring(0, sizeOfBlock);
			key = StringToByteFormat(key);

		}
		private void CorrectKey(ref string key)
		{
			int a = 0;
			int b = 0;
			StringBuilder output = new StringBuilder(key);
			foreach (var ch in key)
			{
				b++;
				if (ch == '1')
				{
					a++;
				}
				if (b % 8 == 0)
				{
					a = 0;
					if (a % 2 != 0)
					{
						output[b - 1] = '1';
					}
				}
			}
			key = output.ToString();
		}
		private string StringToByteFormat(string input)
		{
			//�����, ����������� ������ � �������� ������
			string output = "";
			byte[] bytes = Encoding.GetEncoding(code).GetBytes(input);
			for (int i = 0; i < input.Length; i++)
			{
				string char_binary = Convert.ToString(bytes[i], 2);
				while (char_binary.Length < sizeOfSymbol)
					char_binary = "0" + char_binary;
				output += char_binary;
			}

			return output;
		}


		private string BitToStringFormat(string input)
		{
			byte[] bytes = new byte[sizeOfBlock];
			//�����, ���������� �������� ��� � �����
			string output = "";
			byte a = 0;
			int i = 0;
			while (input.Length > 0)
			{
				string char_binary = input.Substring(0, sizeOfSymbol);
				input = input.Remove(0, sizeOfSymbol);

				a = Convert.ToByte(char_binary, 2);
				bytes[i] = a;
				i++;
			}
			output = Encoding.GetEncoding(code).GetString(bytes);

			return output;

		}
		 private string RandomString()
		{
			int length = 64;
			Random _random = new Random(Environment.TickCount);
			string chars = "01";
			StringBuilder builder = new StringBuilder(length);

			for (int i = 0; i < length; ++i)
				builder.Append(chars[_random.Next(chars.Length)]);
			return BitToStringFormat(builder.ToString());

		}
		private string Encode_DES_One_Round(string input, string key)
		{
			string L = input.Substring(0, input.Length / 2);
			string R = input.Substring(input.Length / 2, input.Length / 2);
			return (R + XOR(L, f(R, key)));
		}
		private string Decode_DES_One_Round(string input, string key)
		{
			string L = input.Substring(0, input.Length / 2);
			string R = input.Substring(input.Length / 2, input.Length / 2);
			return (XOR(f(L, key), R) + L);

		}
		private string f(string s1, string key)
		{
			string output;
			s1 = Expansion(s1);
			output = XOR(s1, key);
			StringToSBlocks(output);
			output = "";
			for (int i = 0; i < 8; i++)
			{
				SBlocks[i] = Work_S_block(SBlocks[i], i);
				
				output += SBlocks[i];
			}
			output = shiftP(output);
			return output;
		}
		private string XOR(string s1, string s2)
		{

			//����� ��� �������� XOR
			string result = "";


			for (int i = 0; i < s1.Length; i++)
			{
				bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
				bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

				if (a ^ b)
					result += "1";
				else
					result += "0";
			}
			return result;
		}
		private void Encode()
		{
			Key = richTextBox5.Text;
			richTextBox3.Text = StringToByteFormat(S);

			S = StringToNormalLength(S);
			StringToBlocks(S);
			keytransform(ref Key);
			S = "";
			for (int i = 0; i < Blocks.Length; i++)
			{

				Blocks[i] = beginIP(Blocks[i]);
				for (int l = 0; l < 16; l++)
				{
					Key = Perestanovka_key_2(Key);
					KEY[i, l] = Key;
					Blocks[i] = Encode_DES_One_Round(Blocks[i], Key);
					Key = KeyToPrevRound(Key, l);

				}
				Blocks[i] = EndIP(Blocks[i]);
				S += BitToStringFormat(Blocks[i]);
			}

			richTextBox2.Text = S;
		}
		private void Output()
		// ����� � ����
		{
			using (FileStream fstream = new FileStream("out1.txt", FileMode.OpenOrCreate))
			{
				// ����������� ������ � �����
				byte[] array = Encoding.GetEncoding(code).GetBytes(S);
				// ������ ������� ������ � ����
				fstream.Write(array, 0, array.Length);

			}
			Process.Start("out1.txt");
		}
		private void Input()
		{
			//���� �� �����
			using (FileStream fstream = File.OpenRead("out1.txt"))
			{
				// ����������� ������ � �����
				byte[] array = new byte[fstream.Length];
				// ��������� ������
				fstream.Read(array, 0, array.Length);
				// ���������� ����� � ������
				S = Encoding.GetEncoding(code).GetString(array);
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			//�����������
			Input();
			StringToBlocks(S);
			S = "";
			for (int i = 0; i < Blocks.Length; i++)
			{

				Blocks[i] = beginIP(Blocks[i]);
				for (int l = 0; l < 16; l++)
				{

					Blocks[i] = Decode_DES_One_Round(Blocks[i], KEY[i, 15 - l]);

				
				}
				Blocks[i] = EndIP(Blocks[i]);
				S += BitToStringFormat(Blocks[i]);

			}
			for (int i = 0; i < S.Length; i++)
			{
				if (S[i] == '#')
				{
					S = S.Remove(i);
				}
			}
			File.Delete("out1.txt");
			richTextBox4.Text = S;
			richTextBox6.Text = StringToByteFormat(S);
			StreamWriter sw = new StreamWriter("out2.txt");
			sw.WriteLine(S);
			sw.Close();
			Process.Start("out2.txt");
		}

		private void button2_Click(object sender, EventArgs e)
		{
			//����������
			S = richTextBox1.Text;
			Encode();
			Output();

		}
		private void button3_Click(object sender, EventArgs e)
		{
			//����������
			S = "";
			OpenFileDialog read = new OpenFileDialog
			{
				Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
			};
			if (read.ShowDialog() == DialogResult.OK)
			{
				S = File.ReadAllText(read.FileName, Encoding.GetEncoding(code));
			}
			richTextBox1.Text = S;
			Encode();
			Output();
		}

		private void Button4_Click(object sender, EventArgs e)
		{
			richTextBox5.Text = RandomString();
		}
	}
}
